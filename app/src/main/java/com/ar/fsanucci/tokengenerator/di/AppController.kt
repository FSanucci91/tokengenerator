package com.ar.fsanucci.tokengenerator.di

import com.ar.fsanucci.tokengenerator.core.BaseApplication
import org.koin.android.ext.android.startKoin

class AppController: BaseApplication() {
    override fun onInit() {
        startKoin(this, listOf(appModule))
    }
}