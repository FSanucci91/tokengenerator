package com.ar.fsanucci.tokengenerator.di

import com.ar.fsanucci.tokengenerator.ui.viewmodel.AddNewTokenNameViewModel
import com.ar.fsanucci.tokengenerator.ui.viewmodel.GenerateTokenViewModel
import com.ar.fsanucci.tokengenerator.ui.viewmodel.MainTokenGeneratorViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val appModule = module {
    viewModel { MainTokenGeneratorViewModel() }
    viewModel { AddNewTokenNameViewModel() }
    viewModel { GenerateTokenViewModel() }
}
