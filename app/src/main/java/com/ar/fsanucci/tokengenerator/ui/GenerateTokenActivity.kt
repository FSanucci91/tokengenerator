package com.ar.fsanucci.tokengenerator.ui

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.ar.fsanucci.tokengenerator.R
import com.ar.fsanucci.tokengenerator.core.BaseActivity
import com.ar.fsanucci.tokengenerator.ui.viewmodel.GenerateTokenViewModel
import kotlinx.android.synthetic.main.activity_generate_token.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class GenerateTokenActivity : BaseActivity(), AdapterView.OnItemSelectedListener {

    private val viewModel: GenerateTokenViewModel by viewModel()
    private val spinnerData = mutableListOf<String>()

    override fun layout(): Int = R.layout.activity_generate_token

    override fun init() {
        spinnerData.add(getString(R.string.generate_token_spinner_prompt))
    }

    override fun setupListeners() {
        val spinnerAdapter =
            ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, spinnerData)
        spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spinnerTokenName.adapter = spinnerAdapter
        spinnerTokenName.onItemSelectedListener = this
    }

    override fun setupObservers() {
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        if (p2 > 0) {
            //Ignores the default value
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, GenerateTokenActivity::class.java)
            context.startActivity(starter)
        }
    }
}