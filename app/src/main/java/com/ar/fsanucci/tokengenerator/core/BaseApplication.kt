package com.ar.fsanucci.tokengenerator.core

import android.app.Application

abstract class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        onInit()
    }

    abstract fun onInit()
}
