package com.ar.fsanucci.tokengenerator.ui

import android.content.Context
import android.content.Intent
import com.ar.fsanucci.tokengenerator.R
import com.ar.fsanucci.tokengenerator.core.BaseActivity
import com.ar.fsanucci.tokengenerator.ui.viewmodel.AddNewTokenNameViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddNewTokenNameActivity : BaseActivity() {

    private val viewModel: AddNewTokenNameViewModel by viewModel()

    override fun layout(): Int = R.layout.activity_add_new_token_name

    override fun init() {
    }

    override fun setupListeners() {
    }

    override fun setupObservers() {
    }

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, AddNewTokenNameActivity::class.java)
            context.startActivity(starter)
        }
    }
}