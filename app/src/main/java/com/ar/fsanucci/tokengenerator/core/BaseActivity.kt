package com.ar.fsanucci.tokengenerator.core

import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.ar.fsanucci.tokengenerator.R

abstract class BaseActivity : AppCompatActivity() {

    @LayoutRes
    abstract fun layout(): Int

    abstract fun init()

    abstract fun setupListeners()

    abstract fun setupObservers()

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_AppCompat_Light_NoActionBar)
        super.onCreate(savedInstanceState)
        setContentView(layout())
        init()
        setupListeners()
        setupObservers()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}