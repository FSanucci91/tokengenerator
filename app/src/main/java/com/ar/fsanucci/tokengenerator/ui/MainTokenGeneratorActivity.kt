package com.ar.fsanucci.tokengenerator.ui

import com.ar.fsanucci.tokengenerator.R
import com.ar.fsanucci.tokengenerator.core.BaseActivity
import com.ar.fsanucci.tokengenerator.ui.viewmodel.MainTokenGeneratorViewModel
import kotlinx.android.synthetic.main.activity_main_token_genrator.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainTokenGeneratorActivity : BaseActivity() {
    private val viewModel: MainTokenGeneratorViewModel by viewModel()

    override fun layout(): Int = R.layout.activity_main_token_genrator

    override fun init() {
    }

    override fun setupListeners() {
        btnGenerate.setOnClickListener {
            GenerateTokenActivity.start(this)
        }

        btnAddToken.setOnClickListener {
            AddNewTokenNameActivity.start(this)
        }
    }

    override fun setupObservers() {
    }
}